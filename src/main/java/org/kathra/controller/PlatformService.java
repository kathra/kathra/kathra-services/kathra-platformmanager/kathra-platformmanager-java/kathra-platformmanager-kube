/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.controller;

import org.kathra.model.Platform;
import org.kathra.model.KathraResponse;

import java.util.List;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public interface PlatformService {

    List<Platform> listPlatforms(String owner) throws Exception;

    KathraResponse deletePlatform(String platformName) throws Exception;

    Platform getPlatform(String platformName) throws Exception;

    KathraResponse deployPlatform(Platform platform) throws Exception;
}
