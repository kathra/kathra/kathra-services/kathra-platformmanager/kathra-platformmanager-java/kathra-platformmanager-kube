/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.controller;

import org.kathra.model.Platform;
import org.kathra.model.Service;
import org.kathra.model.KathraNotification;
import org.kathra.model.Subscriber;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.client.*;
import io.swagger.annotations.ApiModel;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
@ApiModel(description = "Represents a platform proxy")
public class PlatformProxy extends RouteBuilder {

    public static final String UPDATE_PLATFORM = "updatePlatform";
    public static final String OPERATION = "operation";
    public static final String PLATFORM_NAME = "platformName";
    public static final String WEBSOCKET_CONNECTION_KEY = "websocket.connectionKey";
    public static final String NAMESPACE = "namespace";
    public static final String AVAILABLE_SERVICES = "availableServices";
    public static final String FAILSAFE_SERVICES = "failsafeServices";
    public static final String ERROR_SERVICES = "errorServices";
    public static final String DEPLOYING_SERVICES = "deployingServices";
    Platform platform;
    ConcurrentHashMap<String, Subscriber> subscribers = new ConcurrentHashMap();
    ConcurrentHashMap<String, Watch> watches = new ConcurrentHashMap();
    KubernetesClient client = new DefaultKubernetesClient();

    public final Logger logger;
    static ProducerTemplate pt;

    public PlatformProxy(Platform platform) {
        this.platform = platform;
        logger = LoggerFactory.getLogger("PlatformProxy(" + platform.getName() + ')');
        watchNamespace();
        watchEvents();
    }

    @Override
    public void configure() {

        initializeProducerTemplate(getContext());

        from("timer:init?delay=-1&repeatCount=1")
                .routeId(platform.getName())
                .process(exchange -> deployServices())
                .end();
    }

    private static void initializeProducerTemplate(CamelContext context) {
        if (pt == null) {
            pt = context.createProducerTemplate();
            pt.setDefaultEndpointUri("seda:websocketOutput?blockWhenFull=true");
        }
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public void addSubscriber(Subscriber subscriber) {
        synchronized (subscribers) {
            //TODO: develop permissions backend and use client here to know available platforms for user
            if ("all".equals(subscriber.getUsername()) && !subscribers.containsKey(subscriber.getWsKey())) {
                subscribers.put(subscriber.getWsKey(), subscriber);
                subscriber.subscribe(platform.getName());
            }
        }
    }

    public void removeSubscriber(String wsKey) {
        synchronized (subscribers) {
            subscribers.remove(wsKey);
        }
    }

    public void removeSubscriber(Subscriber subscriber) {
        removeSubscriber(subscriber.getWsKey());
    }

    public ConcurrentMap<String, Subscriber> getSubscribers() {
        return subscribers;
    }

    public void onClose() throws InterruptedException {
        watches.values().forEach(Watch::close);
        Thread.sleep(2000);
        int watchesSize = getSynchWatchesSize();

        while (watchesSize > 0) {
            Thread.sleep(1000);
            watches.values().forEach(Watch::close);
            logger.info("Waiting for watches to be closed : {}", watches.keys());
            watchesSize = getSynchWatchesSize();
        }
        logger.info("Closed all watches");
    }

    public void watchEvents() {

        Watch watch = client.events().inNamespace(platform.getName()).watch(new Watcher<Event>() {
            @Override
            public void eventReceived(Action action, Event resource) {
                sendToAllSubscribers(resource, "newEvent");

                if (resource.getReason().equals("Started")) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        log.error("InterruptedException caught in lambda", e);
                        Thread.currentThread().interrupt();
                    }
                    updatePlatformStatus(false);
                } else if (!action.equals(Action.DELETED)) {
                    updatePlatformStatus(false);
                }
            }

            @Override
            public void onClose(KubernetesClientException e) {
                logger.info("Closing event watch");
                if (e != null) {
                    logger.info(e.getMessage(), e);
                } else {
                    synchronized (watches) {
                        watches.remove("event");
                    }
                    logger.info("Closed event watch");
                }
            }
        });
        synchronized (watches) {
            watches.put("event", watch);
        }
        logger.info("Added event watch");
    }

    public void watchNamespace() {

        Watch watch = client.namespaces().withName(platform.getName()).watch(new Watcher<Namespace>() {
            int deleteCount = 3;

            @Override
            public void eventReceived(Action action, Namespace resource) {
                logger.debug("NS :" + resource);
                if (resource.getStatus().getPhase().equalsIgnoreCase("Terminating")) {
                    if (deleteCount == 1) {
                        platform.setStatus(Platform.Status.DELETED);
                        sendToAllSubscribers(new KathraNotification(KathraNotification.Status.info, "Platform " + platform.getName() + " has been deleted."), "deletePlatform");
                        deleteProxy();
                    } else if (deleteCount == 3) {
                        platform.setStatus(Platform.Status.DELETING);
                        platform.getServices().parallelStream().forEach(s ->
                                s.setStatus(Service.Status.DELETING)
                        );
                        sendToAllSubscribers(platform, UPDATE_PLATFORM);
                    }
                    deleteCount--;
                }
            }

            @Override
            public void onClose(KubernetesClientException e) {
                logger.info("Closing namespace watch");
                if (e != null) {
                    logger.info(e.getMessage(), e);
                } else {
                    synchronized (watches) {
                        watches.remove(NAMESPACE);
                    }
                    logger.info("Closed namespace watch");
                }
            }
        });

        synchronized (watches) {
            watches.put(NAMESPACE, watch);
        }
        logger.info("Added namespace watch");
    }

    private void sendToAllSubscribers(Object body, String operation) {
        HashMap headers = new HashMap();
        headers.put(OPERATION, operation);
        headers.put(PLATFORM_NAME, platform.getName());
        for (Subscriber s : subscribers.values()) {
            headers.put(WEBSOCKET_CONNECTION_KEY, s.getWsKey());
            pt.sendBodyAndHeaders(body, headers);
        }
    }

    public void sendPfToSubscriber(Subscriber s) {
        HashMap headers = new HashMap();
        headers.put(OPERATION, UPDATE_PLATFORM);
        headers.put(PLATFORM_NAME, platform.getName());
        headers.put(WEBSOCKET_CONNECTION_KEY, s.getWsKey());
        pt.sendBodyAndHeaders(platform, headers);
    }

    private void deleteProxy() {
        pt.asyncSend("bean:SPMRouteBuilder?method=onDelete(${header.platformName})",
                exchange -> exchange.getIn().setHeader(PLATFORM_NAME, platform.getName()));
    }

    private void updateServiceStatus(String serviceName, boolean waitForURL) {
        Service s = platform.getService(serviceName);
        try {
            if (s == null) {
                logger.info("No service with name {}", serviceName);
                Thread.sleep(5000);
                return;
            }

            String url = s.getUrl();

            if (url == null || url.isEmpty()) {
                do {
                    Thread.sleep(5000);
                    url = getServiceUrl(s.getId());
                    if (url == null) {
                        logger.info("No service with name {}", s.getId());
                        return;
                    }
                } while (waitForURL && url.isEmpty());
                if (!url.isEmpty()) s.setUrl(url);
            }
            updateServiceStatus(s);
        } catch (InterruptedException e) {
            log.error("InterruptedException caught in lambda", e);
            Thread.currentThread().interrupt();
        }
    }

    private void updateServiceStatus(Service s) {
        Map<String, String> selector = client.services().inNamespace(platform.getName()).withName(s.getId()).get().getSpec().getSelector();

        PodList podList = client.pods().inNamespace(platform.getName()).withLabels(selector).list();

        if (podList == null) return;

        List<Pod> items = podList.getItems();

        if (items == null || items.isEmpty()) return;

        int expectedPods = items.size();
        int runningPods = (int) items.stream().filter(pod -> pod.getStatus().getPhase().equalsIgnoreCase("running")).count();

        if (runningPods == expectedPods) s.setStatus(Service.Status.AVAILABLE);
        else if (!s.getStatus().equals(Service.Status.DEPLOYING)) {
            if (runningPods < expectedPods && runningPods >= 1) s.setStatus(Service.Status.FAILSAFE);
            else if (runningPods == 0) s.setStatus(Service.Status.ERROR);
        }
    }

    private String getServiceUrl(String serviceName) {
        io.fabric8.kubernetes.api.model.Service s = client.services().inNamespace(platform.getName()).withName(serviceName).get();
        if (s == null) return null;

        ObjectMeta metadata = s.getMetadata();

        if (metadata == null) return "";

        Map<String, String> labels = metadata.getLabels();

        if (labels == null) return "";

        String expose = labels.get("expose");
        if (expose != null && expose.equalsIgnoreCase("true")) {
            logger.info("Service({}) Getting url", serviceName);
            Map<String, String> annotations = metadata.getAnnotations();
            if (annotations != null) {
                String serviceUrl = annotations.get("fabric8.io/exposeUrl");
                if (serviceUrl != null && !serviceUrl.isEmpty()) {
                    logger.info("Service({}) URL OK :", serviceUrl);
                    return serviceUrl;
                } else return "";
            }
        } else {
            logger.info("Service({}) Not getting url, Internal Service", serviceName);
            return "Internal Service";
        }

        return "";
    }

    private void updatePlatformStatus(boolean waitForURL) {

        Platform.Status status = platform.getStatus();

        if (platform.getStatus().equals(Platform.Status.DELETING)) {
            return;
        }

        Map<String, AtomicInteger> statusMap = new ConcurrentHashMap();
        statusMap.put(AVAILABLE_SERVICES, new AtomicInteger(0));
        statusMap.put(FAILSAFE_SERVICES, new AtomicInteger(0));
        statusMap.put(ERROR_SERVICES, new AtomicInteger(0));
        statusMap.put(DEPLOYING_SERVICES, new AtomicInteger(0));

        AtomicBoolean changed = new AtomicBoolean(false);

        platform.getServices().parallelStream().forEach((Service s) -> {

            Service.Status oldStatus = s.getStatus();
            updateServiceStatus(s.getId(), waitForURL);

            if (!changed.get() && !s.getStatus().equals(oldStatus)) changed.set(true);

            synchronized (statusMap) {
                if (s.getStatus().equals(Service.Status.AVAILABLE)) {
                    statusMap.get(AVAILABLE_SERVICES).incrementAndGet();
                } else if (s.getStatus().equals(Service.Status.FAILSAFE)) {
                    statusMap.get(FAILSAFE_SERVICES).incrementAndGet();
                } else if (s.getStatus().equals(Service.Status.ERROR)) {
                    statusMap.get(ERROR_SERVICES).incrementAndGet();
                } else if (s.getStatus().equals(Service.Status.DEPLOYING)) {
                    statusMap.get(DEPLOYING_SERVICES).incrementAndGet();
                }
            }
        });

        updatePlateformStatus(statusMap);

        if (!changed.get() && !platform.getStatus().equals(status)) changed.set(true);

        if (changed.get()) {
            sendToAllSubscribers(platform, UPDATE_PLATFORM);
        }
    }

    private void updatePlateformStatus(Map<String, AtomicInteger> statusMap) {
        if (statusMap.get(ERROR_SERVICES).get() > 0) platform.setStatus(Platform.Status.ERROR);
        else if (statusMap.get(DEPLOYING_SERVICES).get() > 0) platform.setStatus(Platform.Status.DEPLOYING);
        else if (statusMap.get(FAILSAFE_SERVICES).get() > 0) platform.setStatus(Platform.Status.FAILSAFE);
        else if (statusMap.get(AVAILABLE_SERVICES).get() > 0) platform.setStatus(Platform.Status.AVAILABLE);
    }

    public void deployServices() throws InterruptedException {
        if (platform.getStatus().equals(Platform.Status.DEPLOYING)) {
            logger.info("Deploying templates");

            int watchesSize = getSynchWatchesSize();

            while (watchesSize < 2) {
                Thread.sleep(1000);
                logger.info("Waiting for watches to be created");
                watchesSize = getSynchWatchesSize();
            }
            pt.sendBody("bean:platformController?method=deployTemplates(${body})", platform);
            Thread.sleep(5000);
            logger.info("Deployed templates");
        }

        Platform pf = (Platform) pt.requestBodyAndHeader("bean:platformController?method=getPlatform(${header.platformName})", null, PLATFORM_NAME, platform.getName());
        pf.setStatus(Platform.Status.DEPLOYING);
        for (Service s : pf.getServices()) {
            s.setStatus(Service.Status.DEPLOYING);
        }
        this.platform = pf;
        updatePlatformStatus(true);
    }

    public int getSynchWatchesSize() {
        int watchesSize;
        synchronized (watches) {
            watchesSize = watches.size();
        }
        return watchesSize;
    }

    public List getEvents() {
        return client.events().inNamespace(platform.getName()).list().getItems();
    }
}