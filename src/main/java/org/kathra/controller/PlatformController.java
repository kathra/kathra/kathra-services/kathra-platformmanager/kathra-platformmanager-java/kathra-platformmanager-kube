/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.controller;

import com.hubspot.jinjava.Jinjava;
import org.kathra.Config;
import org.kathra.catalogmanager.client.CatalogManagerClient;
import org.kathra.model.Platform;
import org.kathra.model.Service;
import org.kathra.model.KathraResponse;
import org.kathra.model.Template;
import org.kathra.platform.model.KathraCatalogResource;
import org.kathra.platform.model.KathraEnvironmentResource;
import org.kathra.platform.model.KathraEnvironmentResourceParameter;
import org.kathra.utils.annotations.Eager;
import org.kathra.utils.serialization.YamlUtils;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.Watch;
import io.fabric8.kubernetes.client.Watcher;
import io.fabric8.kubernetes.client.dsl.FilterWatchListDeletable;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */

@Named("platformController")
@Eager
@ApplicationScoped
public class PlatformController implements PlatformService {

    public static final String OWNER = "owner";
    public static final String KIND = "kind";
    static Config config = new Config();
    static String kathraPlatformLabel = "kathra-platform";
    static Jinjava jinjava = new Jinjava();
    static final Logger logger = LoggerFactory.getLogger("PlatformController");
    static KubernetesClient client = new DefaultKubernetesClient();
    static CatalogManagerClient catalogClient = new CatalogManagerClient(config.getCatalogManagerUrl());

    public List<Platform> listPlatforms(String owner) throws Exception {

        LinkedList<Platform> platforms = new LinkedList();

        NamespaceList nss = client.namespaces()
                .withLabel(OWNER, owner)
                .withLabel(KIND, kathraPlatformLabel)
                .list();

        if (null == nss.getItems()) return platforms;

        for (Namespace ns : nss.getItems()) {

            Platform pf = getPlatform(ns.getMetadata().getName());

            platforms.add(pf);
        }

        return platforms;
    }

    public List<String> getAllPlatformsIds() {
        NamespaceList nss = client.namespaces()
                .withLabel(KIND, kathraPlatformLabel)
                .list();
        LinkedList<String> platforms = new LinkedList();

        if (null != nss.getItems()) {
            platforms.addAll(nss.getItems().stream().map(ns -> ns.getMetadata().getName()).collect(Collectors.toList()));
        }

        return platforms;
    }

    public KathraResponse deletePlatform(String platformName) {
        FilterWatchListDeletable<Namespace, NamespaceList, Boolean, Watch, Watcher<Namespace>> ns = client.namespaces()
                .withLabel("name", platformName)
                .withLabel(KIND, kathraPlatformLabel);

        NamespaceList list = ns.list();

        if (list.getItems() == null || list.getItems().isEmpty())
            return new KathraResponse(KathraResponse.Status.failure, "Platform '" + platformName + "' doesn't exists");
        else if (list.getItems().size() > 1) {
            return new KathraResponse(KathraResponse.Status.failure, "Multiple entries for Platform '" + platformName + "' seems to exist, please contact your administrator");
        } else {
            ns.delete();
            return new KathraResponse(KathraResponse.Status.success, "Platform '" + platformName + "' has been successfully deleted");
        }
    }

    public boolean checkIfPlatformExists(String platformName) {
        NamespaceList nss = client.namespaces()
                .withLabel("name", platformName)
                .list();

        return ((nss.getItems() != null) && !nss.getItems().isEmpty());
    }

    public Platform getPlatform(String platformName) throws Exception {
        NamespaceList nss = client.namespaces()
                .withLabel("name", platformName)
                .withLabel(KIND, kathraPlatformLabel)
                .list();

        if (null == nss.getItems()) throw new Exception("This platform doesn't exist");

        Namespace namespace = nss.getItems().get(0);

        Platform pf = new Platform(platformName, namespace.getMetadata().getLabels().get(OWNER));

        ServiceList slist = client.services().inNamespace(platformName).list();
        List<Service> services = new LinkedList();
        pf.setStatus(Platform.Status.AVAILABLE);

        if (slist.getItems() != null && !slist.getItems().isEmpty()) {
            for (io.fabric8.kubernetes.api.model.Service s : slist.getItems()) {
                Service service = new Service().name(s.getMetadata().getName()).id(s.getMetadata().getName()).status(Service.Status.AVAILABLE);
                if (s.getMetadata().getAnnotations() != null && s.getMetadata().getAnnotations().get("kathra/exposeUrl") != null) {
                    service.setUrl(s.getMetadata().getAnnotations().get("kathra/exposeUrl"));
                }
                PodList podList = client.pods().inNamespace(platformName).withLabel("project", service.getId()).list();
                if (podList.getItems() != null && !podList.getItems().isEmpty()) {
                    for (Pod pod : podList.getItems()) {
                        if (!pod.getStatus().getPhase().equalsIgnoreCase("running")) {
                            service.setStatus(Service.Status.ERROR);
                            break;
                        }
                    }
                }
                services.add(service);
            }
        }

        pf.setServices(services);

        for (Service service : services) {
            if (service.getStatus() != Service.Status.AVAILABLE) {
                pf.setStatus(Platform.Status.ERROR);
                break;
            }
        }

        pf.setTags(getTagsFromNamespace(namespace));

        return pf;
    }

    private List<String> getTagsFromNamespace(Namespace namespace) {
        if (namespace.getMetadata().getAnnotations() != null && namespace.getMetadata().getAnnotations().containsKey("tags"))
            return Arrays.asList(namespace.getMetadata().getAnnotations().get("tags").split(","));
        else
            return new ArrayList();
    }

    public String renderTemplate(KathraEnvironmentResource ser, String yamlTemplate) {

        Map<String, Object> context = new HashMap();

        if (ser.getParameters() != null && !ser.getParameters().isEmpty()) {
            for (KathraEnvironmentResourceParameter parameter : ser.getParameters()) {
                context.put(parameter.getName(), parameter.getValue());
            }
        }

        return jinjava.render(yamlTemplate, context);
    }

    public void deployTemplates(Platform platform) throws Exception {

        Map<String, Object> map;

        for (Service service : platform.getServices()) {
            logger.info("Deploying SER : " + service.getId());
            String yamlTemplate = catalogClient.getCatalogResourceTemplate(service.getId());
            Template template = platform.getTemplate(service.getId());

            if (service.getParameters() != null && !service.getParameters().isEmpty()) {
                for (KathraEnvironmentResourceParameter envParam : service.getParameters()) {
                    if (envParam.getValue() == null || envParam.getValue().isEmpty()) {
                        envParam.setValue(template.getParameter(envParam.getName()).getDefaultValue());
                    }
                }
                KathraEnvironmentResource ser = new KathraEnvironmentResource().parameters(service.getParameters());
                ser.setId(service.getId());
                ser.setName(service.getName());
                ser.setCatalogResource(service.getId());
                map = YamlUtils.yamlToMap(renderTemplate(ser, yamlTemplate));
            } else {
                map = YamlUtils.yamlToMap(yamlTemplate);
            }

            //TODO: Check for missing keys

            List<Map<String, Object>> objects = (List<Map<String, Object>>) map.get("objects");
            HasMetadata k8sResource = null;
            boolean deployCephSecret = false;
            for (Map<String, Object> obj : objects) {
                String kind = (String) obj.get(KIND);

                switch (kind) {
                    case "Service":
                        k8sResource = client.services().load(IOUtils.toInputStream(YamlUtils.yaml.dump(obj), "UTF-8")).get();
                        ObjectMeta metadata = k8sResource.getMetadata();
                        if (metadata == null) {
                            metadata = new ObjectMeta();
                            k8sResource.setMetadata(metadata);
                        }
                        Map<String, String> labels = metadata.getLabels();
                        if (labels != null && !labels.isEmpty()) {
                            String expose = labels.get("expose");
                            if (expose != null && expose.equals("true")) {
                                deployServiceIngress(platform.getName(), metadata.getName());
                            }
                        }

                        Map<String, String> annotations = k8sResource.getMetadata().getAnnotations();
                        if (annotations == null) {
                            annotations = new LinkedHashMap();
                            k8sResource.getMetadata().setAnnotations(annotations);
                        }
                        k8sResource.getMetadata().getAnnotations().put("kathra/exposeUrl", "http://" + metadata.getName() + "." + platform.getName() + "." + config.getTopLevelDomain());

                        break;
                    case "Deployment":
                        k8sResource = client.extensions().deployments().load(IOUtils.toInputStream(YamlUtils.yaml.dump(obj), "UTF-8")).get();
                        break;
                    case "Ingress":
                        //k8sResource = client.extensions().ingresses().load(IOUtils.toInputStream(yaml.dump(obj))).get();
                        continue;
                    case "Secret":
                        k8sResource = client.secrets().load(IOUtils.toInputStream(YamlUtils.yaml.dump(obj), "UTF-8")).get();
                        break;
                    case "ConfigMap":
                        k8sResource = client.configMaps().load(IOUtils.toInputStream(YamlUtils.yaml.dump(obj), "UTF-8")).get();
                        break;
                    case "PersistentVolumeClaim":
/*                        k8sResource = client.persistentVolumeClaims().load(IOUtils.toInputStream(yaml.dump(obj))).get();
                        if (!deployCephSecret && annotations.containsKey("volume.beta.kubernetes.io/storage-class")) {
                            deployCephSecret = true;
                        }*/
                        break;
                    default:
                        throw new IllegalArgumentException("BUG With K8sResource Template");
                }
                client.resource(k8sResource).inNamespace(platform.getName()).createOrReplace();
                logger.info("[" + service.getId() + "]: Deployed " + k8sResource.getKind() + " " + k8sResource.getMetadata().getName());
            }
            if (deployCephSecret) {
                logger.info("Deploying ceph-secret");
                HashMap data = new HashMap();
                data.put("key", "QVFDTnlwbFljS1FnRmhBQW42MVg5SkpFa3JpTFh1UHJLUFpHTGc9PQ==");
                client.secrets().inNamespace(platform.getName()).createNew().withNewMetadata().withName("ceph-secret").endMetadata().withType("kubernetes.io/rbd").withData(data).done();
                logger.info("Deployed ceph-secret");
            }
        }
    }

    private void deployServiceIngress(String platformName, String serviceName) {
        client.extensions().ingresses()
                .inNamespace(platformName).createNew().withNewMetadata()
                .withName(serviceName)
                .addToAnnotations("kubernetes.io/ingress.class", config.getIngressControllerName())
                .addToLabels("ingress", "plain").endMetadata()
                .withNewSpec().addNewRule()
                .withHost(serviceName + "." + platformName + "." + config.getTopLevelDomain())
                .withNewHttp().addNewPath().withNewBackend()
                .withServiceName(serviceName)
                .withServicePort(new IntOrString(80)).endBackend().endPath().endHttp().endRule().endSpec().done();
    }

    public KathraResponse deployPlatform(Platform platform) throws Exception {
        Namespace ns = client.namespaces().withName(platform.getName()).get();

        if (ns == null) {
            String tagsLabel = null;
            if (platform.getTags() != null) {
                tagsLabel = String.join(",", platform.getTags());
            }

            ns = client.namespaces()
                    .createNew()
                    .withNewMetadata()
                    .withName(platform.getName())
                    .addToLabels(OWNER, platform.getOwner())
                    .addToLabels(KIND, kathraPlatformLabel)
                    .addToLabels("name", platform.getName())
                    .addToAnnotations("tags", tagsLabel)
                    .endMetadata()
                    .done();

            if (tagsLabel == null) ns.getMetadata().setAnnotations(null);

            applyLimitRangesToNamespace(platform.getName());
        } else {
            throw new Exception("A platform with the same name is already existing");
        }

        platform.setStatus(Platform.Status.DEPLOYING);

        platform.setServices(platform.getServices());

        for (Service service : platform.getServices()) {
            KathraCatalogResource catalogResource = catalogClient.getCatalogResource(service.getId());
            service.setStatus(Service.Status.DEPLOYING);
            platform.addTemplate(new Template(service.getId(), catalogResource.getParameters()));
        }

        return new KathraResponse(KathraResponse.Status.success, "Platform '" + platform.getName() + "' is being deployed");
    }

    public void applyLimitRangesToNamespace(String namespaceName) throws Exception {
        Namespace ns = client.namespaces().withName(namespaceName).get();
        if (ns != null) {
            LimitRange limitRange = new LimitRange();
            LimitRangeSpec limitRangeSpec = new LimitRangeSpec();
            LimitRangeItem lri = new LimitRangeItem();
            HashMap defaultLimits = new HashMap();
            defaultLimits.put("cpu", new Quantity("1000m"));
            defaultLimits.put("memory", new Quantity("2048Mi"));
            HashMap requestLimits = new HashMap();
            requestLimits.put("cpu", new Quantity("2000m"));
            requestLimits.put("memory", new Quantity("4096Mi"));
            lri.setDefault(defaultLimits);
            lri.setMaxLimitRequestRatio(requestLimits);
            lri.setType("Container");
            List<LimitRangeItem> lriList = new ArrayList();
            lriList.add(lri);
            limitRangeSpec.setLimits(lriList);
            limitRange.setSpec(limitRangeSpec);
            client.limitRanges().inNamespace(namespaceName).create(limitRange);
        } else throw new Exception("No namespace with name " + namespaceName);
    }
}
