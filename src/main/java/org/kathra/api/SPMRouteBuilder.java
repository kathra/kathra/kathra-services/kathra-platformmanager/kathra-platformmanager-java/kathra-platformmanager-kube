/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.api;

import org.kathra.Config;
import org.kathra.catalogmanager.client.CatalogManagerClient;
import org.kathra.controller.PlatformProxy;
import org.kathra.model.Platform;
import org.kathra.model.KathraResponse;
import org.kathra.model.Subscriber;
import org.kathra.model.WebsocketMessage;
import org.kathra.utils.serialization.GsonUtils;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.websocket.WebsocketComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Define REST services using the Camel REST DSL
 */
@ContextName("SPM")
@Named("SPMRouteBuilder")
@ApplicationScoped
public class SPMRouteBuilder extends RouteBuilder {
    static Config config = new Config();
    public static final String WEBSOCKET_CONNECTION_KEY = "websocket.connectionKey";
    public static final String OPERATION = "operation";
    public static final String PLATFORM_NAME = "platformName";

    static String inWsInputUri = "seda:websocketInput?concurrentConsumers=5";
    static String inWsOutputUri = "seda:websocketOutput?concurrentConsumers=5";

    static String outWsInputUri = "seda:websocketInput?blockWhenFull=true";
    static String outWsOutputUri = "seda:websocketOutput?blockWhenFull=true";
    static ConcurrentHashMap<String, Subscriber> subscribers = new ConcurrentHashMap();
    static ConcurrentHashMap<String, PlatformProxy> proxies = new ConcurrentHashMap();
    static Instant lastCleanUp = Instant.now();
    public static final Logger logger = LoggerFactory.getLogger("KathraPlatformManager");
    static CatalogManagerClient catalogClient = new CatalogManagerClient(config.getCatalogManagerUrl());

    @Override
    public void configure() {

        WebsocketComponent websocket = getContext().getComponent("websocket", WebsocketComponent.class);
        websocket.setMaxThreads(50);

        websocket.setPort(config.getWsPort());

        onException(SocketTimeoutException.class).handled(true).process(exchange -> {
            Map<String, Object> headers = exchange.getIn().getHeaders();
            logger.info("Headers " + headers);
        });

        onException(Exception.class).handled(true).setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500)).process(exchange -> {
            Exception ex = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
            KathraResponse se = new KathraResponse(KathraResponse.Status.failure, ex.getMessage());
            ex.printStackTrace();
            exchange.getIn().setBody(se);
        }).choice().when(header(WEBSOCKET_CONNECTION_KEY).isNotNull()).to("seda:sendMessageToClient?blockWhenFull=true").endChoice().end();

        from("timer:subscribersCleanUp?delay=80s&period=80s")
                .routeId("subscribersCleanUp")
                .process((exchange) -> {
                    onCleanUp();
                    logger.debug("CLEANUP");
                    synchronized (proxies) {
                        synchronized (subscribers) {
                            logger.debug("");
                            logger.debug("=========== SUBS =============");
                            for (Subscriber s : subscribers.values()) {
                                HashMap map = new HashMap();
                                map.put("wsKey", s.getWsKey());
                                map.put("pfs", s.getSubscribedPlatforms());
                                logger.debug("=========== " + s.getUsername() + " =============");
                                logger.debug("LastSeen " + ((Duration.between(s.getLastSeen(), Instant.now()).toMillis()) / 1000) + "seconds ago");
                                logger.debug(map.toString());
                            }
                            logger.debug("");
                            logger.debug("=========== PFS =============");
                            for (PlatformProxy pr : proxies.values()) {
                                HashMap map = new HashMap();
                                map.put("name", pr.getPlatform().getName());
                                map.put("subs", GsonUtils.gson.toJson(pr.getSubscribers()));
                                logger.debug("=========== " + pr.getPlatform().getName() + " =============");
                                logger.debug(map.toString());
                            }
                        }
                    }
                })
                .stop().end();

        // Handle the incoming WS messags
        from("websocket://spm")
                .routeId("websocket")
                .to(outWsInputUri);

        from(inWsOutputUri)
                .routeId("sedawebsocketOutput")
                .process(this::marshalWebsocketMessage)
                .to("websocket://spm");

        //@formatter:off
        from(inWsInputUri)
                .routeId("sedawebsocketInput")
                .process(this::unmarshalWebsocketMessage)
                .choice()
                /** Connection **/
                .when(header(OPERATION).isEqualTo("connection"))
                .inOnly("seda:sendServiceListToClient?blockWhenFull=true")
                .to("bean:platformController?method=listPlatforms(${header.username})")
                .setHeader(OPERATION, constant("listPlatforms"))
                .to(outWsOutputUri)
                .process(this::onConnect)
                .stop()
                .endChoice()

                /** keepAlive **/
                .when(header(OPERATION).isEqualTo("keepAlive"))
                .process(this::onKeepAlive)
                .endChoice()

                /** createPlatform **/
                .when(header(OPERATION).isEqualTo("createPlatform"))
                .process(exchange -> transformBody(exchange, Platform.class))
                .setHeader(PLATFORM_NAME, simple("${body.name}"))
                .setHeader("platform", simple("${body}"))
                .to("bean:platformController?method=deployPlatform(${body})")
                .inOnly("seda:sendMessageToClient?blockWhenFull=true")
                .setBody(simple("${header.platform}"))
                .removeHeader("platform")
                .inOnly(outWsOutputUri)
                .process(this::onPlatformCreation)
                .stop()
                .endChoice()

                /** getEventsLogger **/
                .when(header(OPERATION).isEqualTo("getEventsLogger"))
                .setHeader(PLATFORM_NAME, simple("${header.platformName}"))
                .setHeader(OPERATION, simple("events"))
                .process(exchange -> {
                    String platformName = exchange.getIn().getHeader(PLATFORM_NAME, String.class);
                    exchange.getIn().setBody(proxies.get(platformName).getEvents());
                })
                //.inOnly("seda:sendMessageToClient?blockWhenFull=true")
                .inOnly(outWsOutputUri)
                .stop()
                .endChoice()

                /** deletePlatform **/
                .when(header(OPERATION).isEqualTo("deletePlatform"))
                .to("bean:platformController?method=deletePlatform(${header.platformName})")
                .stop()
                .endChoice()
                .end()
                .to(outWsOutputUri);

        from("seda:sendPlatformListToClient?concurrentConsumers=5")
                .routeId("sendPlatformListToClient")
                .setHeader(OPERATION, constant("listPlatforms"))
                .to("bean:platformController?method=listPlatforms(${header.username})")
                .to(outWsOutputUri);

        from("seda:sendServiceListToClient?concurrentConsumers=5")
                .routeId("sendServiceListToClient")
                .setHeader(OPERATION, constant("listServices"))
                .process(exchange -> {
                    exchange.getIn().setBody(catalogClient.getAllCatalogResources());
                })
                .to(outWsOutputUri);

        from("seda:sendMessageToClient?concurrentConsumers=5")
                .routeId("sendMessageToClient")
                .setHeader(OPERATION, constant("message"))
                .to(outWsOutputUri);
    }

    private void onPlatformCreation(Exchange exchange) throws Exception {
        Platform pf = exchange.getIn().getBody(Platform.class);
        String wsKey = exchange.getIn().getHeader(WEBSOCKET_CONNECTION_KEY, String.class);
        if (wsKey == null)
            throw new NullPointerException("Something is broken here, please contact your administrator.");
        if (pf == null) throw new NullPointerException("Something is broken here, please contact your administrator.");
        if (pf.getName() == null || pf.getName().isEmpty())
            throw new NullPointerException("Expecting non-empty platformName.");

        synchronized (proxies) {
            synchronized (subscribers) {
                if (!proxies.containsKey(pf.getName()) && subscribers.containsKey(wsKey)) {
                    logger.info("Creating proxy " + pf.getName());
                    pf.setStatus(Platform.Status.DEPLOYING);
                    if (pf.getTags() != null && pf.getTags().isEmpty()) {
                        pf.setTags(null);
                    }
                    PlatformProxy platformProxy = new PlatformProxy(pf);
                    platformProxy.addSubscriber(subscribers.get(wsKey));
                    proxies.put(pf.getName(), platformProxy);
                    getContext().addRoutes(platformProxy);
                    logger.info("Created proxy " + pf.getName());
                    subscribers.wait(500);
                }
            }
        }
    }

    public void onConnect(Exchange exchange) {
        Subscriber subscriber = new Subscriber(exchange.getIn().getHeader("username", String.class), exchange.getIn().getHeader(WEBSOCKET_CONNECTION_KEY, String.class));
        List<Platform> platforms = exchange.getIn().getBody(List.class);
        addSubscriber(subscriber);

        platforms.parallelStream().forEach(pf -> {
            synchronized (proxies) {
                PlatformProxy platformProxy = proxies.get(pf.getName());
                if (platformProxy == null) {
                    logger.info("Creating proxy " + pf.getName());
                    platformProxy = new PlatformProxy(pf);
                    proxies.put(pf.getName(), platformProxy);
                    try {
                        getContext().addRoutes(platformProxy);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    logger.info("Created proxy " + pf.getName());
                }
                platformProxy.addSubscriber(subscriber);
                platformProxy.sendPfToSubscriber(subscriber);
            }
        });
    }

    public void onKeepAlive(Exchange exchange) {
        Subscriber sub = subscribers.get(exchange.getIn().getHeader(WEBSOCKET_CONNECTION_KEY, String.class));
        if (sub != null) {
            sub.updateLastSeen();
        }
    }

    public void onDelete(String pfName) throws Exception {
        PlatformProxy platformProxy = proxies.get(pfName);
        if (platformProxy != null) {
            platformProxy.getSubscribers().values().forEach(s -> s.unSubscribe(pfName));
            platformProxy.onClose();
            getContext().getInflightRepository().removeRoute(pfName);
            getContext().stopRoute(pfName, 5, TimeUnit.SECONDS);
            getContext().removeRoute(pfName);
            proxies.remove(pfName);
        }
    }

    public void marshalWebsocketMessage(Exchange exchange) {
        WebsocketMessage msg = new WebsocketMessage();
        msg.setOperation(exchange.getIn().getHeader(OPERATION, String.class));
        String platformName = exchange.getIn().getHeader(PLATFORM_NAME, String.class);
        if (platformName != null && !platformName.isEmpty()) msg.getHeaders().put(PLATFORM_NAME, platformName);
        msg.setBody(exchange.getIn().getBody());
        exchange.getIn().setBody(GsonUtils.gson.toJson(msg));
    }

    public void unmarshalWebsocketMessage(Exchange exchange) {
        WebsocketMessage msg = GsonUtils.gson.fromJson(exchange.getIn().getBody(String.class), WebsocketMessage.class);
        exchange.getIn().getHeaders().putAll(msg.getHeaders());
        exchange.getIn().setBody(msg);
    }

    public void transformBody(Exchange exchange, Class c) {
        exchange.getIn().setBody(GsonUtils.gson.fromJson(GsonUtils.gson.toJsonTree(exchange.getIn().getBody(WebsocketMessage.class).getBody()).getAsJsonObject(), c));
    }

    public static void addSubscriber(Subscriber s) {
        synchronized (subscribers) {
            subscribers.put(s.getWsKey(), s);
        }
    }

    public void onCleanUp() throws Exception {
        synchronized (subscribers) {
            // Check for inactive subscribers and remove them
            for (Iterator<Subscriber> iterator = subscribers.values().iterator(); iterator.hasNext(); ) {
                Subscriber s = iterator.next();
                if (s.getLastSeen().isBefore(lastCleanUp)) {
                    logger.info("Subscriber " + s.getWsKey() + " : Cleaning " + s.getSubscribedPlatforms());
                    synchronized (proxies) {
                        for (String pfName : s.getSubscribedPlatforms()) {
                            logger.info("Subscriber " + s.getWsKey() + " : Cleaning " + pfName);
                            PlatformProxy platformProxy = proxies.get(pfName);
                            if (platformProxy != null && platformProxy.getPlatform().getStatus() != Platform.Status.DELETING) {
                                platformProxy.removeSubscriber(s);
                                if (platformProxy.getSubscribers().isEmpty()) {
                                    logger.info("Deleting proxy " + pfName);
                                    onDelete(pfName);
                                    logger.info("Deleted proxy " + pfName);
                                }
                            }
                        }
                    }
                    // Remove the inactive subscriber from global subscribers list
                    iterator.remove();
                }
            }
        }

        lastCleanUp = Instant.now();
    }

    private void closeProxy(PlatformProxy platformProxy) throws Exception {
        platformProxy.onClose();
        getContext().removeRoute(platformProxy.getPlatform().getName());
    }
}
