/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra;

import org.kathra.utils.ConfigManager;

public class Config extends ConfigManager {

    private String catalogManagerUrl;
    private String ingressControllerName;
    private String topLevelDomain;
    private int wsPort;

    public Config() {
        catalogManagerUrl = getProperty("KATHRA_CATALOGMANAGER_URL","http://catalogmanager/api/v1");
        ingressControllerName = getProperty("KATHRA_INGRESSCONTROLLER_NAME","traefik");
        topLevelDomain =  getProperty("KATHRA_TOPLEVEL_DOMAIN","irtsystemx.org");
        wsPort = Integer.parseInt(getProperty("WS_PORT","8080"));
    }

    public String getCatalogManagerUrl() {
        return catalogManagerUrl;
    }

    public String getIngressControllerName() {
        return ingressControllerName;
    }

    public String getTopLevelDomain() {
        return topLevelDomain;
    }

    public int getWsPort() {
        return wsPort;
    }
}
