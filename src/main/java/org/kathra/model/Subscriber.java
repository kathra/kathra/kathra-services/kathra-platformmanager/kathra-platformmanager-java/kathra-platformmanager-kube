/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class Subscriber {

    String username;
    String wsKey;
    Instant lastSeen;
    List<String> subscribedPlatforms = new LinkedList();

    public Subscriber(String username, String wsKey) {
        this.username = username;
        this.wsKey = wsKey;
        this.lastSeen = Instant.now();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWsKey() {
        return wsKey;
    }

    public void setWsKey(String wsKey) {
        this.wsKey = wsKey;
    }

    public void subscribe(String pfName) {
        synchronized (subscribedPlatforms) {
            subscribedPlatforms.add(pfName);
        }
    }

    public void unSubscribe(String pfName) {
        synchronized (subscribedPlatforms) {
            subscribedPlatforms.remove(pfName);
        }

    }

    public Instant getLastSeen() {
        return lastSeen;
    }

    public List<String> getSubscribedPlatforms() {
        return subscribedPlatforms;
    }

    public void updateLastSeen() {
        this.lastSeen = Instant.now();
    }
}
