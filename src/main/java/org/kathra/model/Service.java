/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.model;

import org.kathra.platform.model.KathraEnvironmentResourceParameter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
@ApiModel(description = "Represents a service")
public class Service {

    private String name;
    private String url;
    private String version;

    private List<String> categories;
    private String licence;
    private String id;
    private String teaser;
    private String description;
    private String website;
    private Status status;
    private String icon;

    private List<KathraEnvironmentResourceParameter> parameters;
    private Map<String,KathraEnvironmentResourceParameter> parameterIndex=new HashMap();

    public enum Status {
        DEPLOYING, AVAILABLE, DELETING, FAILSAFE, ERROR;
    }

    public Service() {
    }

    public Service(String name, List<KathraEnvironmentResourceParameter> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    public Service(String name) {
        this.name = name;
    }

    public Service(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public Service(String name, String url, String version) {
        this.name = name;
        this.url = url;
        this.version = version;
    }

    @ApiModelProperty(value = "Url of the service")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ApiModelProperty(value = "The name of the Service", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "The version of the Service")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @ApiModelProperty(value = "The categories of the Service")
    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @ApiModelProperty(value = "The shortened description of the Service")
    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    @ApiModelProperty(value = "The full description of the Service")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ApiModelProperty(value = "The vendor website of the Service")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @ApiModelProperty(value = "The shortened description of the Service")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @ApiModelProperty(value = "Parameters", required = true)
    public List<KathraEnvironmentResourceParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<KathraEnvironmentResourceParameter> parameters) {
        this.parameters = parameters;
        parameters.forEach(param -> parameterIndex.put(param.getName(), param));
    }

    @ApiModelProperty(value = "The service's status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @ApiModelProperty(value = "The service's icon url")
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @ApiModelProperty(value = "The service's licence")
    public String getLicence() {
        return licence;
    }

    public void setLicence(String _licence) {
        this.licence = _licence;
    }

    public KathraEnvironmentResourceParameter getParameter(String name) {
        return parameterIndex.get(name);
    }

    public Service name(String name) {
        this.name = name;
        return this;
    }

    public Service url(String url) {
        this.url = url;
        return this;
    }

    public Service version(String version) {
        this.version = version;
        return this;
    }

    public Service license(String licence) {
        this.licence = licence;
        return this;
    }

    public Service id(String id) {
        this.id = id;
        return this;
    }

    public Service teaser(String teaser) {
        this.teaser = teaser;
        return this;
    }

    public Service description(String description) {
        this.description = description;
        return this;
    }

    public Service website(String website) {
        this.website = website;
        return this;
    }

    public Service status(Status status) {
        this.status = status;
        return this;
    }

    public Service icon(String icon) {
        this.icon = icon;
        return this;
    }
}
