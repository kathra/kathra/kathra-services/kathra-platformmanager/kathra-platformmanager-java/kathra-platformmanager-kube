/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
@ApiModel(description = "Represents a platform")
public class Platform {
    String name;
    String owner;
    List<Service> services;
    List<Template> templates = new ArrayList();
    List<String> tags;
    Status status;

    Map<String,Service> serviceIndex=new HashMap();
    Map<String,Template> templateIndex=new HashMap();

    public enum Status {
        DEPLOYING,AVAILABLE,DELETING,DELETED,FAILSAFE,ERROR;
    }

    public Platform() {
    }

    public Platform(String name, String owner) {
        this.name = name;
        this.owner = owner;
    }

    public Platform(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "Name of the platform", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "Owner of the platform", required = true)
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @ApiModelProperty(value = "Services in the platform", required = true)
    public List<Service> getServices() {
        return services;
    }

    @ApiModelProperty(value = "Tags of the platform")
    public List<String> getTags() {
        return tags;
    }

    public Template getTemplate(String templateName) {
        return templateIndex.get(templateName);
    }

    public void setTemplates(List<Template> templates) {
        this.templates = templates;
        templates.forEach(template -> templateIndex.put(template.getName(),template));
    }

    @ApiModelProperty(value = "Templates in the platform", required = true)
    public List<Template> getTemplates() {
        return templates;
    }

    @ApiModelProperty(value = "Templates in the platform", required = true)
    public void addTemplate(Template template) throws Exception {
        if(!templateIndex.containsKey(template.getName())) {
            templates.add(template);
            templateIndex.put(template.getName(),template);
        } else throw new Exception("The template " +template.getName() +" is already in the platform");
    }

    public Service getService(String serviceName) {
        return serviceIndex.get(serviceName);
    }

    public void setServices(List<Service> services) {
        serviceIndex.clear();
        this.services = services;
        services.forEach(service -> serviceIndex.put(service.getId(),service));
    }

    public void setTags(List<String> _tags) { this.tags = _tags; }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}