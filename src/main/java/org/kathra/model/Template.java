/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.model;

import org.kathra.platform.model.KathraCatalogResourceParameter;
import org.kathra.platform.model.KathraEnvironmentResourceParameter;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class Template {

    private String name;

    List<Service> services;
    private List<KathraCatalogResourceParameter> parameters;

    private Map<String, Service> serviceIndex = new HashMap();
    private Map<String,KathraCatalogResourceParameter> parameterIndex=new HashMap();

    public Template() {
    }

    public Template(String name) {
        this.name = name;
    }

    public Template(String name, List<KathraCatalogResourceParameter> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    public Service getService(String serviceName) {
        return serviceIndex.get(serviceName);
    }

    public void setServices(List<Service> services) {
        this.services = services;
        services.forEach(service -> serviceIndex.put(service.getName(), service));
    }

    @ApiModelProperty(value = "Services in the template", required = true)
    public List<Service> getServices() {
        return services;
    }

    @ApiModelProperty(value = "Name of the template", required = true)
    public String getName() {
        return name;
    }

    @ApiModelProperty(value = "Parameters", required = true)
    public List<KathraCatalogResourceParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<KathraCatalogResourceParameter> parameters) {
        this.parameters = parameters;
        parameters.forEach(param -> parameterIndex.put(param.getName(), param));
    }

    public KathraCatalogResourceParameter getParameter(String name) {
        return parameterIndex.get(name);
    }
}
