/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.controller;

import com.google.gson.Gson;
import org.kathra.platform.model.KathraEnvironmentResource;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class PlatformControllerTest {

    PlatformController controller = new PlatformController();
    Gson gson = new Gson();

    @Test
    public void renderTemplate() throws Exception {
        File templates = new File("src/test/resources/templates");
        Path dir = templates.toPath();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path entry : stream) {
                String yamlTemplate;
                try {
                    yamlTemplate = FileUtils.readFileToString(FileUtils.getFile(entry + "/scr-template.yml"));
                    KathraEnvironmentResource ser = gson.fromJson(FileUtils.readFileToString(FileUtils.getFile(entry + "/ser.json")), KathraEnvironmentResource.class);
                    String renderedTemplate = controller.renderTemplate(ser, yamlTemplate);
                    System.out.println("Rendering template " + entry.getFileName());
                    assertEquals("Rendering template " + entry.getFileName(), FileUtils.readFileToString(FileUtils.getFile(entry + "/rendered.yml")), renderedTemplate);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void testAtomicIntegerInHashMap() {

        final String AVAILABLE_SERVICES = "availableServices";

        Map<String, AtomicInteger> statusMap = new ConcurrentHashMap();
        statusMap.put(AVAILABLE_SERVICES, new AtomicInteger(0));

        AtomicBoolean changed = new AtomicBoolean(false);

        synchronized (statusMap) {
            statusMap.get(AVAILABLE_SERVICES).incrementAndGet();
        }

        assertEquals(1, statusMap.get(AVAILABLE_SERVICES).get());
    }
}